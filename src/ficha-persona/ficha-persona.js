import { LitElement, html, css } from 'lit-element';
import { OrigenPersona } from '../origen-persona/origen-persona.js';

class FichaPersona  extends LitElement {

  static get styles() {
    return css`
      .container {
        display: inline-block;
        border: 1px solid black;
        border-radius: 10px;
        padding: 10px;
        margin: 10px;
        width:fit-content;
        height:fit-content;
      }
      .inline{
        display: inline-block;
        margin: 1em;
        vertical-align: middle;
      }
      label{
          display: inline-block;
          text-align: left;
          width: 100px;
      }
    `;
  }

  static get properties() {
    return {
        nombre:{type:String},
        apellidos:{type:String},
        anyosAntiguedad:{type:Number},
        nivel:{type:String},
        foto:{type:Object},
        origen:{type:String},
        bg:{type:String}
    };
  }

  constructor() {
    super();
    this.nombre="Pedro";
    this.apellidos="Lopez Lopez";
    this.anyosAntiguedad = 4;
    this.foto={
        src:"./src/ficha-persona/img/personas3.jpg",
        alt:"Foto Persona"
    }
    this.origen="México";
    this.bg="aliceblue";
  }

  render() {
    return html`
      <div class="container" style="background-color: ${this.bg}">
          <div class="inline">
            <label for="inombre" >Nombre: </label>
            <input type="text" id="inombre" name="inombre" value="${this.nombre}" @input="${this.updateNombre}"/>
            <br />
            <label for="iapellidos" >Apellidos: </label>
            <input type="text" id="iapellidos" name="iapellidos" value="${this.apellidos}"/>
            <br />
            <label for="iantiguedad" >Antiguedad: </label>
            <input type="number" id="iantiguedad" name="iantiguedad" value="${this.anyosAntiguedad}" @input="${this.updateAntiguedad}"/>
            <br />
            <label for="inivel" >Nivel: </label>
            <input type="text" id="inivel" name="inivel" value="${this.nivel}" disabled/>
            <br />
            <label>Origen:</label>
            <origen-persona @origen-set="${this.origenChange}"></origen-persona>
          </div>
          <div class="inline">
            <img src="${this.foto.src}" height="200" width="200" alt="${this.foto.alt}" style="object-fit: cover"/>
          </div>
      </div>
    `;
  }
  updated(changedProperties) {
    if(changedProperties.has("nombre")){
      console.log("Propiedad nombre cambiada. Valor anterior: "+ changedProperties.get("nombre") +" "+ this.nombre);
    }
    if(changedProperties.has("anyosAntiguedad")){
      this.updateNivel();
    }
  }
  updateNombre(e){
    this.nombre = e.target.value;
  }
  updateAntiguedad(e){
    this.anyosAntiguedad = e.target.value;
  }
  updateNivel(){
    if(this.anyosAntiguedad >= 7){
      this.nivel = "Lider";
    }
    else if(this.anyosAntiguedad >= 5){
      this.nivel = "Senior";
    }
    else if(this.anyosAntiguedad >= 3){
      this.nivel = "Team";
    }
    else{
      this.nivel = "Junior";
    }
  }
  origenChange(e){
    this.origen = e.detail.message;
    if(this.origen === "U"){
      this.bg = "pink";
    }
    else if(this.origen === "M"){
      this.bg = "lightgreen";
    }
    else if(this.origen === "C"){
      this.bg = "lightyellow";
    }
  }
}

customElements.define('ficha-persona', FichaPersona);