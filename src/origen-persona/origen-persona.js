import { LitElement, html, css } from 'lit-element';

export class OrigenPersona  extends LitElement {

  static get styles() {
    return css`
      select {
        width: 200px;
      }
    `;
  }

  static get properties() {
    return {
        origenes : {type:Array}
    };
  }

  constructor() {
    super();
    this.origenes=["México","USA","Canadá"]
  }

  render() {
    return html`
    <select id="sel" @change="${this.selChange}">
        ${this.origenes.map(i => html`<option value="${i.charAt(0)}" >${i}</option>`)}
    </select>
    `;
  }
  selChange(e){
      let Valor = this.shadowRoot.querySelector("#sel").value;
      let event = new CustomEvent('origen-set',{detail:{message: Valor}});
      this.dispatchEvent(event);
  }
}

customElements.define('origen-persona', OrigenPersona);